docker build ./trips/airports/ -t registry.gitlab.com/itba-brewtopia/kubernetes/airports:latest
docker build ./trips/trips/ -t registry.gitlab.com/itba-brewtopia/kubernetes/trips:latest
docker build ./trips/gateway/ -t registry.gitlab.com/itba-brewtopia/kubernetes/gateway:latest  

echo "Pushing images to registry"

docker push registry.gitlab.com/itba-brewtopia/kubernetes/airports:latest
docker push registry.gitlab.com/itba-brewtopia/kubernetes/trips:latest
docker push registry.gitlab.com/itba-brewtopia/kubernetes/gateway:latest

echo "Done"
