## 73.40 - Arquitectura-de-Microservicios - Trabajo práctico Kubernetes

### Instituto Tecnológico de Buenos Aires

## Autores

- Rodríguez, Manuel Joaquín - Legajo 60258
- Arca, Gonzalo - Legajo 60303
- Borinsky, Camila - Legajo 60083
- Jerusalinsky, Agustín - Legajo 60406

## Requisitos previos

### Docker

Instalar docker siguiendo los pasos:

https://docs.docker.com/engine/install/

### Minikube

Instalar minikube, una implementación de kubernetes local siguiendo los pasos:

https://minikube.sigs.k8s.io/docs/start/

### Kubectl

Instalar kubectl, la herramienta de línea de comandos para interactuar con el cluster de kubernetes siguiendo los pasos:

https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/

### Helm

Instalar helm, el package manager para kubernetes siguiendo los pasos:

https://helm.sh/docs/intro/install/

## Instrucciones de uso

### Deploy del Helm Chart

1. Clonar repositorio
2. Ingresar a la carpeta `trips-chart`
3. Ejecutar `helm install trips-chart . --create-namespace --namespace trips`
4. Para ver los pods y las configuraciones desplegadas por el chart ejecutar `kubectl get all -n trips`
5. Para acceder al dashboard de minikube `minikube dashboard`
6. Para bajar los pods y las configuraciones desplegadas por el chart ejecutar `kubectl delete namespace trips`

## Especificaciones Técnicas

- Se debe desplegar esta versión: https://github.com/itba-fcastaneda/73.40-Arquitectura-de-Microservicios/tree/main/clase-09/trips-health

**Imagenes de docker**

Con el script `./build.sh` disponibilizamos las imagenes de los servicios para poder consumir desde kubernetes. Esto ya fue hecho y no hace falta volver a correrlo, es solamente a modo informativo.

- Debe incluir redundancia de pods para los servicios basados en Python.

**Redundancia de pods**

A los deployment de los servicios basados en python se les agrego un replicaset con 3 replicas para que haya redundancia de pods. Esto está parametrizado en el archivo `values.yaml` del chart.

- Todos los archivos de configuración deben ser definidos usando ConfigMap o Secrets.

**Variables de ambiente**

Dado que algunas variables de ambiente eran secretos y otras no decidimos tratarlas por separado. Por esto varios servicios tienen su `config.yml` y también `secret.yml`. En la práctica deberíamos encriptar los secretos usando una herramienta de terceros como podría ser `Secrets manager (AWS)`.

Las variables de tipo `REDIS_HOST` y `REDIS_PORT` decidimos ponerlas en los archivos de configuración del servicio de Redis para que su actualización sea controlada y que quienes consuman el servicio tengan la versión más actualizada. De otra forma, ante un cambio tendría que actualizarse en cada lugar donde se consumen estas variables.

- Se debe elegir el mejor método de despliegue de los pods para cada uno de los servicios.

**Despliegue de pods**

Elegimos el tipo de despliegue `Deployment` para todos los servicios. Esto nos permite tener un control sobre los pods y poder actualizarlos de forma controlada. También nos permite tener un control sobre la cantidad de pods que se despliegan y la cantidad de replicas que se despliegan de cada uno.

- Exponer la API rest por medio de un NodePort.

**Gateway**

Para exponer el servicio `gateway` al exterior usamos `NodePort` indicando como puerto a exponer el `30100`. Esto también está parametrizado en el `values.yml` del chart.

- El servicio de Redis debe disponer de persistencia. Pueden usar un NodePort o local con las consideraciones de affinity correspondientes.

**Persistencia entre ejecuciones**

Para probar que funcionen correctamente los volumenes persistences, probamos tanto reiniciando los pods como eliminandolos. Primero creamos un aeropuerto, y luego con el ID retornado hacemos un GET para verificar que continue persistido.

```bash
curl --request POST "http://$NODE_IP:$NODE_PORT/airport" \
--header 'Content-Type: application/json' \
--data-raw '{
    "airport": "Aeropuerto Internacional de Ezeiza"
}'
```

Reinicio

```bash
kubectl rollout restart deployment -n trips
```

Recreación

```bash
kubectl delete all --all -n trips
helm upgrade trips-chart . -n trips
```

```bash
curl --request GET "http://$NODE_IP:$NODE_PORT/airport/$AIRPORT_ID" \
--header 'Content-Type: application/json'
```

## Ejemplos

Como especificamos en la sección de GATEWAY, el servicio expone el puerto 30100, por lo tanto la variable `NODE_PORT` la configuramos para tener ese valor.

Por default el valor de `NODE_IP` es `192.168.49.2`, pero lo podemos verificar con el siguiente comando:

```bash
kubectl get nodes -o wide
```

Luego configuramos las variables de entorno

```bash
export NODE_IP=192.168.49.2
export NODE_PORT=30100
```

### Crear Airport

```bash
curl --request POST "http://$NODE_IP:$NODE_PORT/airport" \
--header 'Content-Type: application/json' \
--data-raw '{
    "airport": "Aeropuerto Internacional de Ezeiza"
}'
```

```bash
curl --request POST "http://$NODE_IP:$NODE_PORT/airport" \
--header 'Content-Type: application/json' \
--data-raw '{
    "airport": "Miami International Airport"
}'
```

### Buscar Airport por ID

```bash
curl --request GET "http://$NODE_IP:$NODE_PORT/airport/$AIRPORT_ID" \
--header 'Content-Type: application/json'
```

### Crear Trip

```bash
curl --request POST "http://$NODE_IP:$NODE_PORT/trip" \
--header 'Content-Type: application/json' \
--data-raw '{
    "airport_from": "Aeropuerto Internacional de Ezeiza",
    "airport_to": "Miami International Airport"
}'
```

```bash
curl --request GET "http://$NODE_IP:$NODE_PORT/trip/$TRIP_ID" \
--header 'Content-Type: application/json'
```
